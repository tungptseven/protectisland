// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager"

const { ccclass, property } = cc._decorator

@ccclass
export default class Island extends cc.Component {
    private _isAttacked: boolean = false

    public get isAttacked(): boolean {
        return this._isAttacked
    }

    public set isAttacked(value: boolean) {
        this._isAttacked = true
    }

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(other, self) {
        if (other.tag == 1) {
            other.node.destroy()
        } else {
            // other.node.destroy()
        }
        this.node.parent.getComponent('GameLayout').onAttacked()
        GameManager.instance.onAttacked()
    }

    // onLoad () {}

    start() {

    }

    update(dt) {
        GameManager.instance.onCheckHeart()
    }
}
