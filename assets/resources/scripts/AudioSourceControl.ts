// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

export enum SoundType {
    E_Sound_Fire = 0,
    E_Sound_Die,
    E_Sound_Explode,
    E_Sound_Background
}
@ccclass
export default class AudioSourceControl extends cc.Component {

    @property({ type: cc.AudioClip })
    backgroundMusic: cc.AudioClip = null

    // sound effect when bird flying
    @property({ type: cc.AudioClip })
    fireSound: cc.AudioClip = null

    @property({ type: cc.AudioClip })
    dieSound: cc.AudioClip = null

    @property({ type: cc.AudioClip })
    explodeSound: cc.AudioClip = null

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onStart(isMute: boolean) {
        if (!isMute) {
            cc.audioEngine.playMusic(this.backgroundMusic, true)
        } else { return }
    }

    playSound(type: SoundType) {
        if (type == SoundType.E_Sound_Fire) {
            cc.audioEngine.playEffect(this.fireSound, false)
        }
        else if (type == SoundType.E_Sound_Die) {
            cc.audioEngine.playEffect(this.dieSound, false)
        }
        else if (type == SoundType.E_Sound_Explode) {
            cc.audioEngine.playEffect(this.explodeSound, false)
        }
    }

    pause() {
        cc.audioEngine.pauseMusic()
    }

    resume() {
        cc.audioEngine.resumeMusic()
    }
    // update (dt) {}
}
