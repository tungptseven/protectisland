import { eGameState, Define } from "./Define"

export default class GameManager {

    private static _instance: GameManager = null
    private _gameState: eGameState = 0
    private _score: number = 0
    private _heart: number = 0
    private _level: number = 0

    countShip: number = 0
    countPlane: number = 0

    public static get instance(): GameManager {
        if (GameManager._instance == null) {
            GameManager._instance = new GameManager()
            // GameManager._instance.init()
        }
        return GameManager._instance
    }

    public get score(): number {
        return this._score
    }

    public set score(value: number) {
        this._score = value
        cc.Canvas.instance.node.emit(Define.Event.UPDATE_SCORE)
    }

    public get level(): number {
        return this._level
    }

    public set level(value) {
        this._level = value
        cc.Canvas.instance.node.emit(Define.Event.UPDATE_LEVEL)
    }

    public get heart(): number {
        return this._heart
    }

    public set heart(value: number) {
        this._heart = value
        cc.Canvas.instance.node.emit(Define.Event.UPDATE_HEART)
    }

    public get gameState(): eGameState {
        return this._gameState
    }

    public set gameState(value: eGameState) {
        this._gameState = value
    }

    initGame(isReplay: boolean = true) {
        if (isReplay) {
            this.score = 0
            this.heart = Define.HEART_NUMBER
        }
        this.countShip = 500
        this.countPlane = 300
    }

    onHit(isPlane: boolean) {
        if (isPlane) {
            this.score += Define.SCORE_PER_PLANE
        } else {
            this.score += Define.SCORE_PER_SHIP
        }
    }

    onAttacked() {
        this.heart -= 1
    }

    onCheckHeart() {
        if (this.heart == 0) {
            cc.Canvas.instance.node.emit(Define.Event.END_GAME)
        }
    }

}
