// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { Define } from "./Define"
import GameManager from "./GameManager"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

enum ePlaneState {
    init = 0,
    idle = 1,
    moveOut = 2,
    die = 3,
}

@ccclass
export default class Plane extends cc.Component {
    gameControl: GameScene = null

    @property(cc.Animation)
    anim: cc.Animation = null

    _timerIdle: number = 0
    private _isShot: boolean = false
    private _state: ePlaneState = ePlaneState.init

    public get isShot(): boolean {
        return this._isShot
    }

    public set isShot(value: boolean) {
        if (!this._isShot) {
            this._isShot = value
        }
        this.scheduleOnce(function () {
            this.playDeath()
            GameManager.instance.onHit(true)
        }.bind(this), 0.4)

        this.scheduleOnce(function () {
            this.onRemove()
        }.bind(this), 0.8)
    }

    playDeath() {
        this.node.stopAllActions()
        this.anim.play(this.anim.getClips()[0].name)
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Explode)
    }

    setIdle() {
        this._state = ePlaneState.idle
    }

    onRemove() {
        this.node.removeFromParent()
        this.node.destroy()
    }
    // LIFE-CYCLE CALLBACKS:


    onLoad() {
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
    }

    start() {

    }

    update(dt) {
        if (this._state == ePlaneState.idle && !this.isShot) {
            this._timerIdle += dt
            if (this._timerIdle >= Define.PLANE_IDLE_TIMER) {
                this._state = ePlaneState.moveOut
                let self = this
                // this.node.runAction(cc.sequence([
                //     cc.moveTo(8, cc.v2(-80, -cc.winSize.height / 2)),
                //     cc.callFunc(function () {
                //         self.onOut()
                //     })]))
                this._timerIdle = 0
            }
        }
    }
}
