export enum eGameState {
    NONE = 0,
    PLAYING = 1
}

export module Define {
    export class Event {
        public static readonly UPDATE_SCORE = 'UPDATE_SCORE'
        public static readonly UPDATE_LEVEL = 'UPDATE_LEVEL'
        public static readonly UPDATE_HEART = 'UPDATE_HEART'

        public static readonly END_GAME = 'END_GAME'
    }

    export let SCORE_PER_SHIP = 10
    export let SCORE_PER_PLANE = 50
    export let HEART_NUMBER = 5

    export let CD_TIMER = 3
    export let SHIP_IDLE_TIMER = 3
    export let PLANE_IDLE_TIMER = 20

}
