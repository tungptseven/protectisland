// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import HomeLayout from "./HomeLayout"
import GameManager from "./GameManager"
import { eGameState, Define } from "./Define"
import GameOver from "./GameOver"
import AudioSourceControl from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class GameScene extends cc.Component {
    isMuted: boolean = false

    @property(GameLayout)
    gameLayout: GameLayout = null

    @property(HomeLayout)
    homeLayout: HomeLayout = null

    @property(cc.Node)
    nTip: cc.Node = null

    @property(cc.Node)
    nEndGame: cc.Node = null

    @property(AudioSourceControl)
    audioSourceControl: AudioSourceControl = null

    onPlay(event: any, customData: string) {
        this.gameLayout.node.active = true
        this.nEndGame.active = false
        this.nTip.active = false
        GameManager.instance.initGame()
        this.gameLayout.initLevel()
        this.audioSourceControl.onStart(this.isMuted)
    }

    onHome(event: any) {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nTip.active = false
        this.nEndGame.active = false
        this.audioSourceControl.onStart(this.isMuted)
    }

    onReplay(event: any) {
        this.gameLayout.node.active = true
        this.homeLayout.node.active = false
        this.nTip.active = false
        this.nEndGame.active = false
        GameManager.instance.initGame()
        this.gameLayout.initLevel()
        this.gameLayout.onRestart()
        this.audioSourceControl.onStart(this.isMuted)
    }

    onOpenTip(event: any) {
        this.nTip.active = true
    }

    onCloseTip(event: any) {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nTip.active = false
        this.nEndGame.active = false
    }

    onEndGame(event: any) {
        this.gameLayout.node.active = false
        this.nEndGame.active = true
        GameManager.instance.gameState = eGameState.NONE
    }

    onSound(event: any) {
        this.isMuted = !this.isMuted
        if (this.isMuted) {
            this.audioSourceControl.pause()
        } else {
            this.audioSourceControl.resume()
        }
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.Canvas.instance.node.on(Define.Event.END_GAME, this.onEndGame, this)

        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
    }

    start() {

    }

    // update (dt) {}
}
