// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager"
import Ship from "./Ship"
import { eGameState, Define } from "./Define"
import Plane from "./Plane"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class GameLayout extends cc.Component {
    gameControl: GameScene = null

    @property(cc.Label)
    lblScore: cc.Label = null

    @property(cc.Prefab)
    heartPrefab: cc.Prefab = null

    heart: cc.Node[] = []

    @property([cc.Node])
    tempShip: cc.Node[] = []

    @property([cc.Node])
    tempPlane: cc.Node[] = []

    @property(cc.Prefab)
    bullet: cc.Prefab = null

    @property(cc.Node)
    enemyPool: cc.Node = null

    _arrShip: cc.PolygonCollider[] = []
    _arrPlane: cc.PolygonCollider[] = []
    _cdTime: number = 0
    _cdPlane: number = 0

    _onUpdateScore(event: any) {
        this.lblScore.string = GameManager.instance.score.toString()
    }

    _registerEvent() {
        this.node.on(cc.Node.EventType.TOUCH_START, this._onDragBegan, this)
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this._onDragMoved, this)
        this.node.on(cc.Node.EventType.TOUCH_END, this._onDragEnded, this)
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onDragCancelled, this)
    }

    _unregisterEvent() {
        this.node.off(cc.Node.EventType.TOUCH_START, this._onDragBegan, this)
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this._onDragMoved, this)
        this.node.off(cc.Node.EventType.TOUCH_END, this._onDragEnded, this)
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onDragCancelled, this)
    }

    _onDragBegan(event: cc.Event.EventTouch) {
        // if (GameManager.instance.bullet) {
        let touchLocation = event.getLocation()
        // this.nTarget.active = true
        // this.nTarget.setPosition(this.nTarget.parent.convertToNodeSpaceAR(touchLocation))
        // }
    }

    _onDragMoved(event: cc.Event.EventTouch) {

        let touchLocation = event.getLocation()
        // this.nTarget.setPosition(this.nTarget.parent.convertToNodeSpaceAR(touchLocation))
    }

    _onDragEnded(event: cc.Event.EventTouch) {
        // if (this.nTarget.active) {
        let touchLocation = event.getLocation()
        // this.nTarget.active = false
        this.onFire(touchLocation)
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Fire)
        // }
    }

    _onDragCancelled(event: cc.Event.EventTouch) {

        cc.log("vao day dragdrop cancel...")
        // this.nTarget.active = false
    }

    onRestart() {
        this.initHeart(Define.HEART_NUMBER)
    }

    randShip(): cc.Node {
        let rand = Math.random() * 2
        if (rand <= 1) {
            rand = 0
        } else {
            rand = 1
        }
        let ship = cc.instantiate(this.tempShip[rand]) as cc.Node
        return ship
    }

    initHeart(numHeart: number) {
        for (let i = 0;i < numHeart;i++) {
            this.heart[i] = cc.instantiate(this.heartPrefab)
            this.node.getChildByName('Status').getChildByName('Heart').addChild(this.heart[i])
            this.heart[i].x = i * -60
            this.heart[i].y = 0
        }
    }

    initShip() {
        let ship = this.randShip()
        ship.active = true
        this.node.getChildByName('EnemyPool').addChild(ship)
        this._arrShip.push(ship.getComponent(cc.PolygonCollider))
        ship.runAction(cc.sequence([
            cc.moveTo(20, cc.v2(-100, ship.position.y)),
            cc.callFunc(function () {
                let com = ship.getComponent(Ship) as Ship
                com.setIdle()
            })]))
    }

    initPlane() {
        let plane = cc.instantiate(this.tempPlane[0]) as cc.Node
        plane.active = true
        this.node.getChildByName('EnemyPool').addChild(plane)
        this._arrPlane.push(plane.getComponent(cc.PolygonCollider))
        plane.runAction(cc.sequence([
            cc.moveTo(15, cc.v2(-1000, plane.position.y)),
            cc.callFunc(function () {
                let com = plane.getComponent(Plane) as Plane
                com.setIdle()
            })
        ]))
    }

    initLevel() {
        this._cdTime = 2
        this._cdPlane = 5
        this._arrPlane = []
        this._arrShip = []
        this.enemyPool.destroyAllChildren()
        GameManager.instance.gameState = eGameState.PLAYING
    }

    onFire(pos: cc.Vec2) {
        let newBullet = cc.instantiate(this.bullet)
        newBullet.setPosition(this.node.getChildByName('Gun').position.x, this.node.getChildByName('Gun').position.y)
        this.node.addChild(newBullet)

        let targetPos = this.node.convertToNodeSpaceAR(pos)
        let actionBy = cc.moveTo(0.4, cc.v2(targetPos.x, targetPos.y))
        let destruction = cc.callFunc(() => {
            newBullet.destroy()
        }, this)
        let sequence = cc.sequence(actionBy, destruction)
        newBullet.runAction(sequence)
        for (let ship of this._arrShip) {
            if (ship && ship.node) {
                let localPos = ship.node.convertToNodeSpaceAR(pos)
                if (cc.Intersection.pointInPolygon(localPos, ship.points)) {
                    let com = ship.node.getComponent(Ship) as Ship
                    com.isShot = true
                }

            }
        }
        for (let plane of this._arrPlane) {
            if (plane && plane.node) {
                let localPos = plane.node.convertToNodeSpaceAR(pos)
                if (cc.Intersection.pointInPolygon(localPos, plane.points)) {
                    let com = plane.node.getComponent(Plane) as Plane
                    com.isShot = true
                }
            }
        }
    }

    onAttacked() {
        this.node.getChildByName('Status').getChildByName('Heart').removeChild(this.heart[0])
        this.heart.splice(0, 1)
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getCollisionManager().enabled = true
        cc.Canvas.instance.node.on(Define.Event.UPDATE_SCORE, this._onUpdateScore, this)
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
    }

    start() { }

    onEnable() {
        this._registerEvent()
        this.lblScore.string = (GameManager.instance.score.toString())
        this.initHeart(Define.HEART_NUMBER)
    }

    update(dt) {
        if (GameManager.instance.gameState == eGameState.PLAYING) {
            if (this._arrShip.length < GameManager.instance.countShip) {
                this._cdTime -= dt
                if (this._cdTime <= 0) {
                    this._cdTime = Define.CD_TIMER * Math.pow(0.9, Math.floor(Math.random() * 2))
                    this.initShip()
                }
            }
            if (this._arrPlane.length < GameManager.instance.countPlane) {
                this._cdPlane -= dt
                if (this._cdPlane <= 0) {
                    this._cdPlane = Define.CD_TIMER * Math.pow(0.9, Math.floor(Math.random() * 3))
                    this.initPlane()
                }
            }
        }
    }
}
