// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager"

const { ccclass, property } = cc._decorator

@ccclass
export default class GameOver extends cc.Component {

    @property(cc.Label)
    lblScore: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    onEnable() {
        this.lblScore.string = GameManager.instance.score.toString()
    }

    start() {

    }

    // update (dt) {}
}
